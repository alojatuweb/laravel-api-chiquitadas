<?php

namespace Tests\Feature;

use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * @return void
     */
    public function test_login_endpoint(): void
    {
        $this->withHeader('Authorization', config('api.token'))
             ->postJson('/api/login', [
                 'email'    => config('api.email'),
                 'password' => config('api.password')
             ])
             ->assertStatus(200)
             ->assertJsonStructure([
                 'token'
             ]);
    }

    /**
     * @return void
     */
    public function test_not_authenticate_to_a_user_with_credentials_invalid(): void
    {
        $credentials = [
            'email'    => config('api.email'),
            'password' => 'secret'
        ];
        $this->assertInvalidCredentials($credentials);
    }

    /**
     * @return void
     */
    public function test_the_email_is_required_for_authenticate(): void
    {
        $credentials = [
            'email'    => null,
            'password' => config('api.password'),
        ];
        $this->assertInvalidCredentials($credentials);
    }

    /**
     * @return void
     */
    public function test_the_password_is_required_for_authenticate(): void
    {
        $credentials = [
            'email'    => config('api.email'),
            'password' => null,
        ];
        $this->assertInvalidCredentials($credentials);
    }
}
