<?php

namespace Tests\Feature;

use App\Models\Quote;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class QuoteEndpointsTest extends TestCase
{
    use WithFaker;

    /**
     * @return void
     */
    public function test_quote_index_endpoint(): void
    {
        $this->getJson('/api/quotes')
             ->assertStatus(200);
    }

    /**
     * @return void
     */
    public function test_quote_store_endpoint(): void
    {
        $user = User::first();
        $headers = [
            'Authorization' => config('api.token'),
        ];
        $payload = [
            'quote' => $this->faker->realText(),
            'userId' => $user->id,
        ];
        $this->postJson('/api/quote', $payload, $headers)
             ->assertStatus(201)
             ->assertJsonStructure([
                 'id',
                 'quote'
             ]);
    }

    /**
     * @return void
     */
    public function test_quote_show_endpoint(): void
    {
        $quote = Quote::latest()->first();
        $this->getJson('/api/quote/'.$quote->id)
             ->assertStatus(200)
             ->assertJsonStructure([
                 'id',
                 'quote'
             ]);
    }

    /**
     * @return void
     */
    public function test_quote_update_endpoint(): void
    {
        $quote = Quote::latest()->first();
        $headers = [
            'Authorization' => config('api.token'),
        ];
        $payload = [
            'quote' => $this->faker->realText(),
        ];
        $this->putJson('/api/quote/'.$quote->id, $payload, $headers)
             ->assertStatus(200)
             ->assertJsonStructure([
                 'id',
                 'quote'
             ]);
    }

    /**
     * @return void
     */
    public function test_quote_destroy_endpoint(): void
    {
        $quote = Quote::latest()->first();
        $headers = [
            'Authorization' => config('api.token'),
        ];
        $payload = [
            'quote' => $quote,
        ];
        $this->deleteJson('/api/quote/'.$quote->id, $payload, $headers)
             ->assertStatus(200)
             ->assertJsonStructure([
                 'status',
                 'quote' => [
                     "id",
                     "quote"
                 ]
             ]);
    }

    /**
     * @return void
     */
    public function test_quote_random_endpoint(): void
    {
        $this->getJson('/api/quotes/avoleorrr')
             ->assertStatus(200)
             ->assertJsonStructure([
                 'id',
                 'quote'
             ]);
    }
}
