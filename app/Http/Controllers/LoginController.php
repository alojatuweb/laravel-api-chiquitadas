<?php

namespace App\Http\Controllers;

use App\Exceptions\AuthenticateException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * @throws AuthenticateException
     */
    public function authenticate(Request $request)
    {
        if (Auth::attempt($request->only('email', 'password')))
        {
            return response()->json([
                'token' => Auth::user()->createToken(Auth::user()->name)->plainTextToken,
            ]);
        }
        throw new AuthenticateException;
    }
}
