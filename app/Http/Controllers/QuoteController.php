<?php

namespace App\Http\Controllers;

use App\Http\Requests\Quote\DestroyRequest;
use App\Http\Requests\Quote\IndexRequest;
use App\Http\Requests\Quote\ShowRequest;
use App\Http\Requests\Quote\StoreRequest;
use App\Http\Requests\Quote\UpdateRequest;
use App\Http\Resources\QuoteResource;
use App\Models\Quote;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use OpenApi\Annotations as OA;

/**
 * @OA\Info(
 *      version = "1.0.0",
 *      title = "API - Chiquitadas",
 *      description = "Documentación para el uso de nuestra API",
 *      @OA\Contact(
 *          email = "webmasterbrak@gmail.com",
 *      ),
 *      @OA\License(
 *         name = "License GPLv3",
 *         url = "https://www.gnu.org/licenses/gpl-3.0.html"
 *     )
 * )
 * @OA\SecurityScheme(
 *     type="http",
 *     description="Token para acceder",
 *     name="Authorization",
 *     in="header",
 *     scheme="bearer",
 *     bearerFormat="JWT",
 *     securityScheme="bearerToken"
 * )
 * @OA\Server(
 *     url = "https://chiquitadas.es",
 *     description = "Entorno de producción de apis"
 * )
 * @OA\ExternalDocumentation(
 *     description = "Find out more about Swagger",
 *     url = "https://swagger.io"
 * )
 */

class QuoteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum')->except(['index', 'show', 'random']);
    }

    /**
     * ¡Asexsuarrr! pincha en el boton Try it out, despues en Execute y revisa el code para saber la peaso de respuesta del servidorrrr.
     *
     * @param IndexRequest $request
     * @return AnonymousResourceCollection
     * @OA\Get(
     *     path = "/api/quotes",
     *     tags = {"citas"},
     *     summary = "Mostrarrrr el listador con todas las citas, por la gloria de tu madre!!",
     *     @OA\Response(
     *         response = 200,
     *         description = "Petición realizadarrr correctamente, tienes el listado con todas las citas a can demor."
     *     ),
     *     @OA\Response(
     *         response = "400",
     *         description = "Jarrrr!!! ha ocurrido un error con la petición, revisa los datos enviados fistro de la pradera."
     *     )
     * )
     */

    public function index(IndexRequest $request): AnonymousResourceCollection
    {
        return QuoteResource::collection(Quote::all());
    }

    /**
     *¡Fistroooo! pincha en el boton Try it out, modifica los datorrr deseados, despues en Execute y revisa el code para saber la respuesta del servidorrrr ¡Cobarderr!.
     *
     * @param StoreRequest $request
     * @return QuoteResource
     * @OA\Post(
     *     path = "/api/quote",
     *     tags = {"citas"},
     *     summary = "Crear una nueva citarrrr",
     *     security={ {"bearerToken":{}} },
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType = "application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property = "quote",
     *                     type = "string"
     *                 ),
     *                @OA\Property(
     *                     property = "userId",
     *                     type = "int"
     *                 ),
     *                 example = {"quote": "Ejemplo duodenal de una nueva peaso cita", "userId": 1}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response = 201,
     *         description = "Petición realizadarrr correctamente, se creo una nueva citarrr.",
     *     ),
     *     @OA\Response(
     *         response = 400,
     *         description = "Jarrrr!!! ha ocurrido un error con la petición, comprueba los datos enviados fistro de la pradera."
     *     ),
     *     @OA\Response(
     *         response = 405,
     *         description = "Para crear una cita debes estar registado. ¡Animale bravído!!"
     *     )
     * )
     */

    public function store(StoreRequest $request)
    {
        $quote = new Quote;

        $quote = $quote->createModel($request);

        return new QuoteResource($quote);
    }

    /**
     * ¡Pecador! pincha en el boton Try it out, introduce el id de la peaso cita deseada, despues en Execute y revisa el code para saber la respuesta del servidor. ¿Te da cuen?
     *
     * @param ShowRequest $request
     * @param Quote $quote
     * @return QuoteResource
     * @OA\Get(
     *     path = "/api/quote/{id}",
     *     tags = {"citas"},
     *     summary = "Mostrar los datos duodenales de una cita",
     *     @OA\Parameter(
     *         description = "¡No puede sé! este parámetro es necesario para la consulta de una citarrr",
     *         in = "path",
     *         name = "id",
     *         required = true,
     *         @OA\Schema(type = "string"),
     *         @OA\Examples(example = "int", value = "1", summary = "Introduce un número de id para la cita y ¡Relájese usterl!.")
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Petición realizadarrr correctamente, se muestran los datos duodenales de una cita."
     *     ),
     *     @OA\Response(
     *         response = 404,
     *         description = "Jarrrr!!! ha ocurrido un error con la petición, no se encontro la peaso de cita."
     *     )
     * )
     */

    public function show(ShowRequest $request, Quote $quote)
    {
        return new QuoteResource($quote);
    }

    /**
     * ¡Hijo míoorrr! pincha en el boton Try it out, modifica los peaso de datos deseados, despues en Execute y revisa el code para saber la respuesta del servidor que trabaja menos que el sastre de Tarzán.
     *
     * @param UpdateRequest $request
     * @param Quote $quote
     * @return QuoteResource
     * @OA\Put(
     *     path = "/api/quote/{id}",
     *     tags = {"citas"},
     *     summary = "Modificar una cita ya existente y ¡Ten cuidadínnn no te hagas pupita en el fistro duodenalll!",
     *     security={ {"bearerToken":{}} },
     *     @OA\Parameter(
     *         description = "¡Aguaaa, aguaaa! este parámetro es necesario para modificar una citarrr",
     *         in = "path",
     *         name = "id",
     *         required = true,
     *         @OA\Schema(type = "string"),
     *         @OA\Examples(example = "int", value = "1", summary = "Introduce un número de id para la cita y ¡Relájese usterl!.")
     *     ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType = "application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property = "quote",
     *                     type = "string"
     *                 ),
     *                @OA\Property(
     *                     property = "userId",
     *                     type = "int"
     *                 ),
     *                 example = {"quote": "Ejemplo duodenal de una peaso cita modificada", "userId": 1}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response = 200,
     *         description = "Petición realizadarrr correctamente, se actualizo la citarrr.",
     *     ),
     *     @OA\Response(
     *         response = 400,
     *         description = "Jarrrr!!! ha ocurrido un error con la petición, comprueba los datos enviados fistro de la pradera."
     *     ),
     *     @OA\Response(
     *         response = 405,
     *         description = "Para modificara una cita debes estar registado. ¡Animale bravído!!"
     *     )
     * )
     */
    public function update(UpdateRequest $request, Quote $quote): QuoteResource
    {
        $quote->quote = $request->quote;

        $quote->updateModel($request);

        return new QuoteResource($quote);
    }

    /**
     * ¡Er putaaarrr! pincha en el boton Try it out, despues en Execute y revisa el code para saber la respuesta del servidor, a güán, a peich, agromenáuer.
     *
     * @param DestroyRequest $request
     * @param Quote $quote
     * @return JsonResponse
     * @OA\Delete(
     *     path = "/api/quote/{id}",
     *     tags = {"citas"},
     *     summary = "Eliminar una cita y ¡Hasta luego Lucasss!",
     *     security={ {"bearerToken":{}} },
     *     @OA\Parameter(
     *         description = "¡Ese fistro danimarl!, este parámetro es necesario para eliminar una citarrrr",
     *         in = "path",
     *         name = "id",
     *         required = true,
     *         @OA\Schema(type = "string"),
     *         @OA\Examples(example = "int", value = "1", summary = "Introduce un número de id para la cita y ¡Relájese usterl!.")
     *     ),
     *     @OA\Response(
     *         response = 204,
     *         description = "Petición realizadarrr correctamente, se elimino la citarrrr."
     *     ),
     *     @OA\Response(
     *         response = "404",
     *         description = "Jarrrr!!! ha ocurrido un error con la petición, no se encontro la peaso de cita."
     *     ),
     *     @OA\Response(
     *         response = 405,
     *         description = "Para eliminar una cita debes estar registado. ¡Animale bravído!!"
     *     )
     * )
     */
    public function destroy(DestroyRequest $request, Quote $quote)
    {
        $status = $quote->deleteModel();

        return response()->json([
            'status' => $status,
            'quote' => new QuoteResource($quote)
        ]);
    }

    /**
     * Eres más feo que robar a Caritas, pincha en el boton Try it out, despues en Execute y revisa el code para saber la respuesta del servidor ¡Hijo míoorrr!.
     *
     * @return QuoteResource
     * @OA\Get(
     *     path = "/api/quotes/avoleorrr",
     *     tags = {"citas"},
     *     summary = "Mostrar una citarrrr de forma aleatoria ¡Me cago en tu muela!",
     *     @OA\Response(
     *         response = 200,
     *         description = "Petición realizadarrr correctamente, tienes una peaso de citarrr elegida al azar."
     *     ),
     *     @OA\Response(
     *         response = "400",
     *         description = "Jarrrr!!! ha ocurrido un error con la petición, comprueba los datos enviados fistro de la pradera."
     *     )
     * )
     */
    public function random()
    {
        return new QuoteResource(Quote::select("*")->inRandomOrder()->first());
    }
}
