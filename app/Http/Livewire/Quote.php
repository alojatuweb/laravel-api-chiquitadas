<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class Quote extends Component
{
    public function render(): Factory|View|Application
    {
        $quoteJson = \App\Models\Quote::select("quote")->inRandomOrder()->first();
        $array = json_decode($quoteJson, true);
        $quote = $array['quote'];

        return view('livewire.quote', compact('quote'));
    }
}
