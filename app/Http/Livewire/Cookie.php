<?php

namespace App\Http\Livewire;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Cookie as AcceptCookiePolicy;
use Livewire\Component;

class Cookie extends Component
{
    /**
     * @var bool
     */
    public $showBannerCookies = true;

    /**
     * @return Factory|View|Application
     */
    public function render(): Factory|View|Application
    {
        $this->is_accept();
        return view('livewire.cookie');
    }

    /**
     * @return void
     *
     * We check if the cookie exists
     */
    public function is_accept(): void
    {
        if (AcceptCookiePolicy::has('AcceptCookies'))
        {
            $this->showBannerCookies = false;
        }
    }

    /**
     * @return void
     *
     * For bottom accept in popup cookies
     */
    public function acceptCookies(): void
    {
        AcceptCookiePolicy::queue('AcceptCookies', 'Accept');
        $this->showBannerCookies = false;
    }

    /**
     * @return void
     *
     * For bottom discart in popup cookies
     */
    public function discartCookies(): void
    {
        $this->showBannerCookies = false;
    }
}
