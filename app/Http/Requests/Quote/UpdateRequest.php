<?php

namespace App\Http\Requests\Quote;

use App\Http\Requests\BaseFormRequest;
use App\Models\Quote;

class UpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return $this->user()->can('update', Quote::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'quote'   => 'required|string|min:2|max:255|unique:quotes'
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'quote.required'   => 'La cita es necesaria',
            'quote.string'     => 'Solo se permite texto para la cita',
            'quote.min'        => 'La cita tiene que tener como mínimo 2 caracteres',
            'quote.max'        => 'La cita puede tener hasta 255 caracteres',
            'quote.unique'     => 'La cita ya existe y no se puede repetir'
        ];
    }
}
