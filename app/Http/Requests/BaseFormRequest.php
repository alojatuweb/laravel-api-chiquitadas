<?php

namespace App\Http\Requests;

use App\Exceptions\JsonAuthorizationException;
use App\Exceptions\JsonValidationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class BaseFormRequest extends FormRequest
{
    /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws JsonAuthorizationException
     */
    protected function failedAuthorization(): void
    {
        throw new JsonAuthorizationException;
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param Validator $validator
     * @return void
     *
     * @throws JsonValidationException
     */
    protected function failedValidation(Validator $validator): void
    {
        throw new JsonValidationException($validator);
    }
}
