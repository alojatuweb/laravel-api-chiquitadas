<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;

class AuthenticateException extends Exception
{
    /**
     * @return bool
     */
    public function report(): bool
    {
        return false;
    }

    /**
     * @param $request
     * @return JsonResponse
     */
    public function render($request): JsonResponse
    {
        return response()->json([
            'message' => 'Tenemorrrr un problemita sesual para autenticarte, revisa los datos fistro de la pradera!!.'
        ], 401);
    }
}
