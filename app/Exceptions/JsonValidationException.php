<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;

class JsonValidationException extends Exception
{
    /**
     * @var Validator
     */
    protected Validator $validator;

    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @return bool
     */
    public function report(): bool
    {
        return false;
    }

    /**
     * @param $request
     * @return JsonResponse
     */
    public function render($request): JsonResponse
    {
        return response()->json([
            'message' => 'Tenemorrrr un error en la validación fistro de la pradera!!.',
            'errors' => $this->validator->errors()
        ], 422);
    }
}
