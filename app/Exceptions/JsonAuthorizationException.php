<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\JsonResponse;

class JsonAuthorizationException extends Exception
{
    /**
     * @return bool
     */
    public function report(): bool
    {
        return false;
    }

    /**
     * @param $request
     * @return JsonResponse
     */
    public function render($request): JsonResponse
    {
        return response()->json([
            'message' => 'No estas autorizadorrrr!!!.'
        ], 403);
    }
}
