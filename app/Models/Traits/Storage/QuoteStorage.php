<?php

namespace App\Models\Traits\Storage;

use App\Models\Quote;

trait QuoteStorage
{
    /**
     * @param $request
     * @return Quote
     */
    public function createModel($request): Quote
    {
        return $this->create($request->only(['quote', 'userId']));
    }

    /**
     * @param $request
     * @return bool
     */
    public function updateModel($request): bool
    {
        return $this->update($request->only('quote'));
    }

    /**
     * @return bool
     */
    public function deleteModel(): bool
    {
        return $this->delete();
    }
}
