<?php

namespace App\Policies;

use App\Models\User;

class QuotePolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function store(User $user): bool
    {
        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user): bool
    {
        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function delete(User $user): bool
    {
        return true;
    }
}
