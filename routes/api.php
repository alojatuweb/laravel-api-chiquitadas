<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\QuoteController;
use Illuminate\Support\Facades\Route;

Route::post('login', [LoginController::class, 'authenticate'])->name('login');

Route::get('quotes', [QuoteController::class, 'index']);
Route::post('quote', [QuoteController::class, 'store']);
Route::get('quote/{quote}', [QuoteController::class, 'show']);
Route::put('quote/{quote}', [QuoteController::class, 'update']);
Route::delete('quote/{quote}', [QuoteController::class, 'destroy']);
Route::get('quotes/avoleorrr', [QuoteController::class, 'random']);
