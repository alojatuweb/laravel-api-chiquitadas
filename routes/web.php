<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('home');
});

Route::get('cookies-policy', function () {
    return view("cookies-policy");
});

Route::get('legal-disclaimer', function () {
    return view("legal-disclaimer");
});
