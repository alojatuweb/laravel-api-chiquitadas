<?php

return [
    'email'    => env('API_USER_EMAIL'),
    'password' => env('API_USER_PASSWORD'),
    'token'    => env('API_USER_TOKEN')
];
