<p align="center"><a href="https://chiquitadas.es" target="_blank"><img src="https://chiquitadas.es/Chiquito-7.png" width="400" alt="Laravel Logo"></a></p>

## Acerca de Chiquitadas

Chiquitadas es una API REST de código abierto que proporciona citas y frases al estilo del gran Chiquito de la Calzada.

## Aprender Chiquitadas

Si quiere aprender sobre el uso de api Chiquitadas, por favor visite [Chiquitadas](https://chiquitadas.es).

## Patrocinadores de Chiquitadas

Si usted está interesado en convertirse en un patrocinador, por favor envíe un correo electrónico a Webmaster Brak via [webmasterbrak@gmail.com](mailto:webmasterbrak@gmail.com).

## Contribución

Gracias por considerar contribuir con api Chiquitadas. Envíe un correo electrónico a Webmaster Brak via [webmasterbrak@gmail.com](mailto:webmasterbrak@gmail.com).

## Vulnerabilidades de seguridad

Si descubre una vulnerabilidad de seguridad en api Chiquitadas, envíe un correo electrónico a Webmaster Brak via [webmasterbrak@gmail.com](mailto:webmasterbrak@gmail.com). Todas las vulnerabilidades de seguridad se abordarán con prontitud.

## Licencia

Chiquitadas es una api de código abierto bajo la licencia [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html).
