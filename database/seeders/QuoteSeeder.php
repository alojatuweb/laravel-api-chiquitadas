<?php

namespace Database\Seeders;

use App\Models\Quote;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class QuoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('quotes')->delete();
        $json = File::get("database/data/quotes.json");
        $data = json_decode($json, true);
        foreach ($data as $obj) {
            Quote::create(array(
                'userId' => '1',
                'quote' => $obj
            ));
        }
    }
}
