<x-layout>
    @section('title', __('Política de cookies'))

    @include('partials.navbar')

    <section class="container max-w-screen-xl mx-auto h-[2880px] mt-24 pt-6">
        <div class="flex justify-center">
            <h2 class="text-4xl">Política de cookies</h2>
        </div>
        <div class="m-6">
            <h3 class="text-3xl">¿Qué son las cookies?</h3>
            <p class="mt-1">En inglés, el término "cookie" significa galleta, pero en el ámbito de la navegación web, una "cookie" es algo completamente distinto. Cuando accede a nuestro Sitio Web, en el navegador de su dispositivo se almacena una pequeña cantidad de texto que se denomina "cookie". Este texto contiene información variada sobre su navegación, hábitos, preferencias, personalizaciones de contenidos, etc...</p>
            <p class="mt-1">Existen otras tecnologías que funcionan de manera similar y que también se usan para recopilar datos sobre tu actividad de navegación. Llamaremos "cookies" a todas estas tecnologías en su conjunto.</p>
            <p class="mt-1">Los usos concretos que hacemos de estas tecnologías se describen en el presente documento.</p>
            <h3 class="text-3xl mt-6">¿Para qué se utilizan las cookies en esta web?</h3>
            <p class="mt-1">Las cookies son una parte esencial de cómo funciona el Sitio Web. El objetivo principal de nuestras cookies es mejorar su experiencia en la navegación. Por ejemplo, para recordar sus preferencias (idioma, país, etc.) durante la navegación y en futuras visitas. La información recogida en las cookies nos permite además mejorar la web, adaptarla a sus intereses como usuario, acelerar las búsquedas que realice, etc..</p>
            <p class="mt-1">En determinados casos, si hemos obtenido su previo consentimiento informado, podremos utilizar cookies para otros usos, como por ejemplo para obtener información que nos permita mostrarle publicidad basada en el análisis de sus hábitos de navegación.</p>
            <h3 class="text-3xl mt-6">¿Para qué NO se utilizan las cookies en esta web?</h3>
            <p class="mt-1">En las cookies que utilizamos no se almacena información sensible de identificación personal como su nombre, dirección, tu contraseña, etc...</p>
            <h3 class="text-3xl mt-6">¿Quién utiliza la información almacenada en las cookies?</h3>
            <p class="mt-1">La información almacenada en las cookies de nuestro Sitio Web es utilizada exclusivamente por nosotros, a excepción de aquellas identificadas más adelante como "cookie de terceros", que son utilizadas y gestionadas por entidades externas que nos proporcionan servicios que mejoran la experiencia del usuario. Por ejemplo las estadísticas que se recogen sobre el número de visitas, el contenido que más gusta, etc... lo suele gestionar Google Analytics.</p>
            <h3 class="text-3xl mt-6">¿Cómo puede evitar el uso de cookies en este Sitio Web?</h3>
            <p class="mt-1">Si prefiere evitar el uso de las cookies, puede RECHAZAR su uso o puede CONFIGURAR las que quiere evitar y las que permite utilizar (en este documento le damos información ampliada al respecto de cada tipo de cookie, su finalidad, destinatario, temporalidad, etc... ).</p>
            <p class="mt-1">Si las ha aceptado, no volveremos a preguntarle a menos que borre las cookies en su dispositivo según se indica en el apartado siguiente. Si quiere revocar el consentimiento tendrá que eliminar las cookies y volver a configurarlas.</p>
            <h3 class="text-3xl mt-6">¿Cómo deshabilito y elimino la utilización de cookies?</h3>
            <p class="mt-1">El Titular muestra información sobre su Política de cookies en el menú del pie de página y en el banner de cookies accesible en todas las páginas del Sitio Web. El banner de cookies muestra información esencial sobre el tratamiento de datos y permite al Usuario realizar las siguientes acciones:</p>
            <ul class="list-disc ml-6">
                <li>ACEPTAR o RECHAZAR la instalación de cookies, o retirar el consentimiento previamente otorgado.</li>
                <li>Obtener información adicional en la página de <a href="{{ url('cookies-policy') }}" class="text-primary hover:text-primary-dark">Política de cookies</a>.</li>
            </ul>
            <p class="mt-1">Para restringir, bloquear o borrar las cookies de este Sitio Web (y las usada por terceros) puede hacerlo, en cualquier momento, modificando la configuración de su navegador. Tenga en cuenta que esta configuración es diferente en cada navegador.</p>
            <p class="mt-1">En los siguientes enlaces encontrará instrucciones para habilitar o deshabilitar las cookies en los navegadores más comunes.</p>
            <ul class="list-disc ml-6">
                <li><a href="https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias?redirectslug=habilitar-y-deshabilitar-cookies-que-los-sitios-we&redirectlocale=es" target="_blank" class="text-primary hover:text-primary-dark">Firefox</a></li>
                <li><a href="https://support.google.com/chrome/answer/95647?hl=es" target="_blank" class="text-primary hover:text-primary-dark">Google Chrome</a></li>
                <li><a href="https://support.microsoft.com/es-es/windows/eliminar-y-administrar-cookies-168dab11-0753-043d-7c16-ede5947fc64d#ie=%22ie-10" target="_blank" class="text-primary hover:text-primary-dark">Internet Explorer</a></li>
                <li><a href="https://support.microsoft.com/es-es/windows/microsoft-edge-datos-de-exploraci%C3%B3n-y-privacidad-bb8174ba-9d73-dcf2-9b4a-c582b4e640dd" target="_blank" class="text-primary hover:text-primary-dark">Microsoft Edge</a></li>
                <li><a href="https://support.apple.com/es-es/HT201265" target="_blank" class="text-primary hover:text-primary-dark">Safari</a></li>
            </ul>
            <h3 class="text-3xl mt-6">¿Qué tipos de cookies se utilizan en esta página web?</h3>
            <p class="mt-1">Cada página web utiliza sus propias cookies. En nuestra web utilizamos las que se indican a continuación:</p>
            <h4 class="text-xl font-semibold mt-6">SEGÚN LA ENTIDAD QUE LO GESTIONA</h4>
            <h5 class="text-lg font-semibold mt-6">Cookies propias:</h5>
            <p class="mt-1">Son aquellas que se envían al equipo terminal del Usuario desde un equipo o dominio gestionado por el propio editor y desde el que se presta el servicio solicitado por el Usuario.</p>
            <h5 class="text-lg font-semibold mt-6">Cookies de terceros:</h5>
            <p class="mt-1">Son aquellas que se envían al equipo terminal del Usuario desde un equipo o dominio que no es gestionado por el editor, sino por otra entidad que trata los datos obtenidos través de las cookies.</p>
            <p class="mt-1">En el caso de que las cookies sean servidas desde un equipo o dominio gestionado por el propio editor, pero la información que se recoja mediante estas sea gestionada por un tercero, no pueden ser consideradas como cookies propias si el tercero las utiliza para sus propias finalidades (por ejemplo, la mejora de los servicios que presta o la prestación de servicios de carácter publicitario a favor de otras entidades).</p>
            <h4 class="text-xl font-semibold mt-6">SEGÚN SU FINALIDAD</h4>
            <h5 class="text-lg font-semibold mt-6">Cookies técnicas:</h5>
            <p class="mt-1">Son aquellas necesarias para la navegación y el buen funcionamiento de nuestro Sitio Web, como por ejemplo, controlar el tráfico y la comunicación de datos, identificar la sesión, acceder a partes de acceso restringido,recordar los elementos que integran un pedido, realizar el proceso de compra de un pedido, gestionar el pago, controlar el fraude vinculado a la seguridad del servicio, realizar la solicitud de inscripción o participación en un evento, contar visitas a efectos de la facturación de licencias del software con el que funciona el servicio del Sitio Web, utilizar elementos de seguridad durante la navegación, almacenar contenidos para la difusión de vídeos o sonido, habilitar contenidos dinámicos (por ejemplo, animación de carga de un texto o imagen) o compartir contenidos a través de redes sociales.</p>
            <h5 class="text-lg font-semibold mt-6">Cookies de análisis:</h5>
            <p class="mt-1">Permiten cuantificar el número de usuarios y así realizar la medición y análisis estadístico de la utilización que hacen los usuarios del Sitio Web. Para ello se analiza su navegación en nuestro Sitio Web con el fin de mejorar la oferta de productos o servicios que le ofrecemos.</p>
            <h5 class="text-lg font-semibold mt-6">Cookies de preferencias o personalización:</h5>
            <p class="mt-1">Son aquellas que permiten recordar información para que el Usuario acceda al servicio con determinadas características que pueden diferenciar su experiencia de la de otros usuarios, como, por ejemplo, el idioma, el número de resultados a mostrar cuando el Usuario realiza una búsqueda, el aspecto o contenido del servicio en función del tipo de navegador a través del cual el Usuario accede al servicio o de la región desde la que accede al servicio, etc.</p>
            <h5 class="text-lg font-semibold mt-6">Publicitarias comportamentales:</h5>
            <p class="mt-1">Son aquellas que, tratadas por nosotros o por terceros, nos permiten analizar sus hábitos de navegación en Internet para que podamos mostrarle publicidad relacionada con su perfil de navegación.</p>
            <h4 class="text-xl font-semibold mt-6">SEGÚN EL PLAZO DE TIEMPO QUE PERMANECEN ACTIVADAS</h4>
            <h5 class="text-lg font-semibold mt-6">Cookies de sesión:</h5>
            <p class="mt-1">Son aquellas diseñadas para recabar y almacenar datos mientras el Usuario accede a una página web.</p>
            <p class="mt-1">Se suelen emplear para almacenar información que solo interesa conservar para la prestación del servicio solicitado por el Usuario en una sola ocasión (por ejemplo, una lista de productos adquiridos) y desaparecen al terminar la sesión.</p>
            <h5 class="text-lg font-semibold mt-6"> Cookies persistentes:</h5>
            <p class="mt-1">Son aquellas en las que los datos siguen almacenados en el terminal y pueden ser accedidos y tratados durante un periodo definido por el responsable de la cookie, y que puede ir de unos minutos a varios años. A este respecto debe valorarse específicamente si es necesaria la utilización de cookies persistentes, puesto que los riesgos para la privacidad podrían reducirse mediante la utilización de cookies de sesión. En todo caso, cuando se instalen cookies persistentes, se recomienda reducir al mínimo necesario su duración temporal atendiendo a la finalidad de su uso. A estos efectos, el Dictamen 4/2012 del GT29 indicó que para que una cookie pueda estar exenta del deber de consentimiento informado, su caducidad debe estar relacionada con su finalidad. Debido a ello, es mucho más probable que se consideren como exceptuadas las cookies de sesión que las persistentes.</p>
            <h5 class="text-lg font-semibold mt-6">Detalle de cookies utilizadas en esta web:</h5>
            <p class="mt-1 mb-36">En estos momentos no usamos cookies de las que informar.</p>
        </div>
    </section>

    <livewire:cookie />

    @include('partials.footer')
</x-layout>
