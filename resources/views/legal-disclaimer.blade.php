<x-layout>
    @section('title', __('Aviso legal'))

    @include('partials.navbar')

    <section class="container max-w-screen-xl mx-auto h-[1480px] mt-24 pt-6">
        <div class="flex justify-center">
            <h2 class="text-4xl">Aviso legal</h2>
        </div>
        <div class="m-6">
            <h3 class="text-3xl">Identificación y Titularidad</h3>
            <p class="mt-1">En cumplimiento del artículo 10 de la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y Comercio Electrónico, el Titular expone sus datos identificativos:</p>
            <ul class="list-disc ml-6">
                <li>Titular:  Iván López Ordorica. </li>
                <li>NIF: 50202716H</li>
                <li>Domicilio:  Rua dos Gorrions, 12, A Coruña - España.</li>
                <li>Correo electrónico:  webmasterbrak@gmail.com</li>
                <li>Sitio Web:  <a href="https://chiquitadas.es" class="text-primary hover:text-primary-dark">https://chiquitadas.es</a></li>
            </ul>
            <h3 class="text-3xl mt-6">Finalidad</h3>
            <p class="mt-1">La finalidad del Sitio Web es: Proporcionar una API REST de Chiquito de la Calzada.</p>
            <h3 class="text-3xl mt-6">Condiciones de Uso</h3>
            <p class="mt-1">La utilización del Sitio Web le otorga la condición de Usuario, e implica la aceptación completa de todas las cláusulas y condiciones de uso incluidas en las páginas:</p>
            <ul class="list-disc ml-6">
                <li><a href="{{ url('legal-disclaimer') }}" class="text-primary hover:text-primary-dark">Aviso legal</a></li>
                <li><a href="{{ url('cookies-policy') }}" class="text-primary hover:text-primary-dark">Política de cookies</a></li>
            </ul>
            <p class="mt-1">A través del Sitio Web, el Titular le facilita el acceso y la utilización de diversos contenidos que el Titular y/o sus colaboradores han publicado por medio de Internet.</p>
            <p class="mt-1">A tal efecto, está obligado y comprometido a NO utilizar cualquiera de los contenidos del Sitio Web con fines o efectos ilícitos, prohibidos en este Aviso Legal o por la legislación vigente, lesivos de los derechos e intereses de terceros, o que de cualquier forma puedan dañar, inutilizar, sobrecargar, deteriorar o impedir la normal utilización de los contenidos, los equipos informáticos o los documentos, archivos y toda clase de contenidos almacenados en cualquier equipo informático propios o contratados por el Titular, de otros usuarios o de cualquier usuario de Internet.</p>
            <h3 class="text-3xl mt-6">Contenidos</h3>
            <p class="mt-1">El Titular ha obtenido la información, el contenido multimedia y los materiales incluidos en el Sitio Web de fuentes que considera fiables, pero, si bien ha tomado todas las medidas razonables para asegurar que la información contenida es correcta, el Titular no garantiza que sea exacta, completa o actualizada. El Titular declina expresamente cualquier responsabilidad por error u omisión en la información contenida en las páginas del Sitio Web.</p>
            <h3 class="text-3xl mt-6">Política de cookies</h3>
            <p class="mt-1">Puede consultar toda la información relativa a la política de recogida y tratamiento de las cookies en la página de <a href="{{ url('cookies-policy') }}" class="text-primary hover:text-primary-dark">Política de Cookies.</a></p>
            <h3 class="text-3xl mt-6">Enlaces a otros sitios Web</h3>
            <p class="mt-1">El Titular puede proporcionarle acceso a sitios Web de terceros mediante enlaces con la finalidad exclusiva de informar sobre la existencia de otras fuentes de información en Internet en las que podrá ampliar los datos ofrecidos en el Sitio Web.</p>
            <p class="mt-1">Estos enlaces a otros sitios Web no suponen en ningún caso una sugerencia o recomendación para que usted visite las páginas web de destino, que están fuera del control del Titular, por lo que el Titular no es responsable del contenido de los sitios web vinculados ni del resultado que obtenga al seguir los enlaces. Asimismo, el Titular no responde de los links o enlaces ubicados en los sitios web vinculados a los que le proporciona acceso.</p>
            <p class="mt-1">El establecimiento del enlace no implica en ningún caso la existencia de relaciones entre el Titular y el propietario del sitio en el que se establezca el enlace, ni la aceptación o aprobación por parte del Titular de sus contenidos o servicios.</p>
            <p class="mt-1">Si accede a un sitio web externo desde un enlace que encuentre en el Sitio Web usted deberá leer la propia política de privacidad del otro sitio web que puede ser diferente de la de este sitio Web.</p>
            <h3 class="text-3xl mt-6">Jurisdicción</h3>
            <p class="mt-1">Este Aviso Legal se rige íntegramente por la legislación española.</p>
            <h3 class="text-3xl mt-6">Contacto</h3>
            <p class="mt-1 mb-36">En caso de que usted tenga cualquier duda acerca de este Aviso Legal o quiera realizar cualquier comentario sobre el Sitio Web, puede enviar un mensaje de correo electrónico a la dirección: webmasterbrak@gmail.com</p>
        </div>
    </section>

    <livewire:cookie />

    @include('partials.footer')
</x-layout>
