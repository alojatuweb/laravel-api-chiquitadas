<div>
    @if($showBannerCookies)
        <div class="grid grid-cols-4 fixed bottom-0 w-full h-36 sm:h-32 p-3 bg-primary sm:mb-6 mb-16">
            <div class="mx-auto">
                <a href="{{ url('/') }}" class="flex items-center cursor-pointer px-2">
                    <!-- Logo-->
                    <div class="rounded bg-primary text-white hover:text-black font-bold w-12 h-12 flex justify-center lg:text-4xl text-lg lg:pt-0.5 pt-2.5">
                        {{ __('C') }}
                    </div>
                    <div class="text-gray-700 hover:text-white lg:text-3xl text-lg font-semibold lg:ml-2 -ml-2">
                        {{ config('app.name') }}
                    </div>
                </a>
            </div>
            <div class="col-span-2 -ml-3 mr-3 text-xs lg:text-sm">
                Usamos cookies propias para mejorar nuestros servicios. Si continúas navegando en esta página, consideramos que aceptas su uso en los términos indicados en la
                <a class="text-primary-soft hover:text-secundary" href="{{ url('cookies-policy') }}" target="_blank">
                    política de cookies.
                </a>
            </div>
            <div class="sm:mt-0 mt-3">
                <button wire:click="acceptCookies" class="lg:w-36 w-24 lg:text-lg text-xs rounded-full bg-primary-soft hover:bg-secundary">Aceptar cookies</button>
                <button wire:click="discartCookies" class="lg:w-36 w-24 lg:text-lg text-xs rounded-full bg-primary-soft hover:bg-secundary mt-3">Rechazar</button>
            </div>
        </div>
    @endif
</div>
