<x-layout>
    @section('title', __('Citas y frases de Chiquito de la Calzada via API'))

    @include('partials.navbar')

    <div x-data="{ open: true }">
        <div class="fixed xl:mt-56 sm:mt-64 mt-16" x-cloak x-show="open">
            <div class="xl:ml-2 ml-1 mb-1" @click.away="open = true">
                <button class="xl:px-3 px-2 xl:py-2 py-1 bg-secundary hover:bg-gray-500 font-serif text-xs rounded-full border-none focus:outline-none" @click="open = false">
                    X
                </button>
            </div>
            <a href="{{ url('https://www.facebook.com/sharer/sharer.php?u=https://chiquitadas.es') }}" target="_blank">
                <div class="bg-[#3B5998] xl:w-12 w-8 xl:p-2 p-1">
                    <i class="fa-brands fa-facebook-f xl:text-2xl text-gl ml-2"></i>
                </div>
            </a>
            <a href="{{ url('https://twitter.com/intent/tweet?text=comparte%20api-chiquitadas%20en%20twitter&url=https%3A%2F%2Fchiquitadas.es&via=WebmasterBrak&hashtags=programación,api,chiquito') }}" target="_blank">
                <div class="bg-[#1DA1F2] xl:w-12 w-8 xl:p-2 p-1">
                    <i class="fa-brands fa-twitter xl:text-2xl text-gl ml-1"></i>
                </div>
            </a>
            <a href="{{ url('https://pinterest.com/pin/create/button/?url=https://chiquitadas.es') }}" target="_blank">
                <div class="bg-[#CB2027] xl:w-12 w-8 xl:p-2 p-1">
                    <i class="fa-brands fa-square-pinterest xl:text-2xl text-gl ml-1"></i>
                </div>
            </a>
            <a href="{{ url('https://www.linkedin.com/shareArticle?url=https://chiquitadas.es/api/documentation') }}" target="_blank">
                <div class="bg-[#0077B5] xl:w-12 w-8 xl:p-2 p-1">
                    <i class="fa-brands fa-linkedin-in xl:text-2xl text-gl ml-1"></i>
                </div>
            </a>
            <a href="{{ url('https://wa.me/?text=https://chiquitadas.es') }}" target="_blank">
                <div class="bg-green-600 xl:w-12 w-8 xl:p-2 p-1">
                    <i class="fa-brands fa-whatsapp xl:text-2xl text-gl ml-1"></i>
                </div>
            </a>
            <a href="{{ url('https://t.me/share/url?url=https%3A%2F%2Fchiquitadas.es') }}" target="_blank">
                <div class="bg-[#27A7E8] xl:w-12 w-8 xl:p-2 p-1">
                    <i class="fa-brands fa-telegram xl:text-2xl text-gl ml-1"></i>
                </div>
            </a>
        </div>
    </div>

    <section class="container max-w-screen-xl mx-auto bg-white h-[380px] mt-12 pt-6">
        <div>
            <img src="{!! asset('Chiquito-7.png') !!}" alt="" class="2xl:h-96 xl:h-96 md:h-80 sm:h-72 h-80 2xl:w-96 sm:float-left sm:ml-0 mx-auto">
            <h2 class="xl:text-3xl lg:text-2xl text-xl sm:pt-24 pt-0 sm:pr-2 px-6 pb-3">
                {{ __('Chiquitadas es una API REST de código abierto que proporciona citas y frases al estilo del gran Chiquito de la Calzada.') }}
            </h2>
            <div class="flex justify-center md:my-12 sm:my-6 my-3">
                <button class="md:mr-12 mr-6 rounded-2xl p-3 bg-primary-soft hover:bg-primary xl:text-3xl lg:text-2xl md:text-xl text-lg xl:w-56 lg:w-48 md:w-36 w-32">
                    <a href="{{ url('api/documentation') }}" target="_blank">
                        {{ __('Pruébame') }}
                    </a>
                </button>
                <button class="rounded-2xl p-3 bg-primary-soft hover:bg-primary xl:text-3xl lg:text-2xl md:text-xl text-lg xl:w-56 lg:w-48 md:w-36 w-32">
                    <a href="{{ url('https://gitlab.com/webmasterbrak') }}" target="_blank">
                        <i class="fa-brands fa-gitlab"></i>
                        {{ __('Fork') }}
                    </a>
                </button>
            </div>
        </div>
    </section>

    <section class="container max-w-screen-xl mx-auto flex justify-center sm:h-28 h-40 sm:mt-0 mt-56 px-3 py-3">
        <div class="animate-bounce mt-3 mx-6 lg:text-2xl text-lg text-primary-dark">
            <livewire:quote />
        </div>
    </section>

    <section class="bg-secundary lg:h-[280px] md:h-[350px] sm:h-[670px] h-[850px] w-full py-6">
        <div class="grid grid-cols-1 md:grid-cols-3 gap-4 max-w-screen-xl mx-auto">
            <div class="mx-auto min-w-full">
                <div class="flex justify-center text-5xl">
                    <i class="fa-solid fa-chart-line"></i>
                </div>
                <div class="flex justify-center">
                    <h3 class="my-3 lg:text-xl md:text-lg text-xl font-bold">
                        {{ __('API muy divertida') }}
                    </h3>
                </div>
                <div class="mx-3 p-1">
                    {{ __('Obtenga divertidas frases y citas de forma individual, masiva o aleatoria. Las risas están aseguradas y se remontan hasta el verano de 1994, cuando todos conocimos a este gran monstruo doudenal.') }}
                </div>
            </div>
            <div class="mx-auto min-w-full">
                <div class="flex justify-center text-5xl">
                    <i class="fa-solid fa-laptop-code"></i>
                </div>
                <div class="flex justify-center">
                    <h3 class="my-3 lg:text-xl md:text-lg text-xl font-bold">
                        {{ __('Funciona del lado del cliente') }}
                    </h3>
                </div>
                <div class="mx-3 p-1">
                    {{ __('Úselo en su aplicación web, móvil o de escritorio. Chiquitadas limita el uso a 60 peticiones por minuto por motivos de seguridad y requiere de una clave API para crear, editar o eliminar las peasooo citas.') }}
                </div>
            </div>
            <div class="mx-auto min-w-full">
                <div class="flex justify-center text-5xl">
                    <i class="fa-solid fa-server"></i>
                </div>
                <div class="flex justify-center">
                    <h3 class="my-3 lg:text-xl md:text-lg text-xl font-bold">
                        {{ __('Implementa donde quieras') }}
                    </h3>
                </div>
                <div class="mx-3 p-1">
                    {{ __('Fácil implementación de la API en entornos Laravel, PHP y servidores, a través del repositorio en GitLab. Consulte todos nuestros servicios de alojamiento web para particulares y empresas en AlojaTuWeb.') }}
                </div>
            </div>
        </div>
    </section>

    <div class="flex justify-center mt-6 mb-12 text-3xl">
        <H3>
            {{ __('Ejemplos') }}
        </H3>
    </div>

    <section class="container flex max-w-screen-xl mx-auto">
        <div class="mx-auto w-full" x-data="{ current: 1 }">
            <div class="flex overflow-hidden h-16 border-b-2 sm:text-2xl">
                <button class="px-4 py-2 w-full" x-on:click="current = 1" x-bind:class="{ 'bg-primary-soft': current === 1 }">
                    {{ __('Consola') }}
                </button>
                <button class="px-4 py-2 w-full" x-on:click="current = 2" x-bind:class="{ 'bg-primary-soft': current === 2 }">
                    {{ __('PHP') }}
                </button>
                <button class="px-4 py-2 w-full" x-on:click="current = 3" x-bind:class="{ 'bg-primary-soft': current === 3 }">
                    {{ __('JavaScript') }}
                </button>
            </div>
            <div x-show="current === 1" class="p-3 text-center mt-6">
                <p class="text-xl mt-6">{{ __('Si copias y pegas este código en una consola, obtendrás una cita aleatoria.') }}</p>
                <p class="text-lg mt-6">curl -X 'GET' 'https://chiquitadas.es/api/quotes/avoleorrr' -H 'accept: */*' -H 'X-CSRF-TOKEN: '</p>
                <img src="{!! asset('Uso-en-consola.png') !!}" alt="" class="w-full h-72 my-16">
            </div>
            <div x-show="current === 2" class="p-3 text-center mt-6">
                <p class="text-xl mt-6">{{ __('Si copias y pegas este código en una archivo PHP, obtendrás una cita aleatoria.') }}</p>
                <p class="text-lg mt-6">$data = json_decode( file_get_contents('https://chiquitadas.es/api/quotes/avoleorrr'), true ); echo $data['quote'];</p>
                <img src="{!! asset('Uso-en-php.png') !!}" alt="" class="w-full h-72 my-16">
            </div>
            <div x-show="current === 3" class="p-3 text-center mt-6">
                <p class="text-xl mt-6">{{ __('Si copias y pegas este código en una archivo JavaScript, obtendrás una cita aleatoria.') }}</p>
                <p class="text-lg mt-6">fetch('https://chiquitadas.es/api/quotes/avoleorrr').then(response => response.json()).then(response => console.log(response.quote))</p>
                <img src="{!! asset('Uso-en-javascript.png') !!}" alt="" class="w-full h-72 my-16">
            </div>
        </div>
    </section>

    <livewire:cookie />

    @include('partials.footer')
</x-layout>
