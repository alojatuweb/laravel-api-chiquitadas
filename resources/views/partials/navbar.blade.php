<nav class="fixed inset-x-0 top-0">
    <div x-data="{showMenu : false}" class="container max-w-screen-xl mx-auto flex justify-between h-20">
        <!-- Brand-->
        <a href="{{ url('/') }}" class="flex items-center cursor-pointer px-2 ml-3">
            <!-- Logo-->
            <div class="rounded bg-primary text-white hover:text-black font-bold w-12 h-12 flex justify-center text-4xl pt-0.5">
                {{ __('C') }}
            </div>
            <div class="text-gray-700 hover:text-primary text-3xl font-semibold ml-2">
                {{ config('app.name') }}
            </div>
        </a>
        <!-- Navbar Toggle Button -->
        <button @click="showMenu = !showMenu" class="block md:hidden h-12 text-gray-700 p-2 rounded hover:border focus:border focus:bg-gray-100 my-2 mr-5" type="button" aria-controls="navbar-main" aria-expanded="false" aria-label="Toggle navigation">
            <svg class="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"></path></svg>
        </button>
        <!-- Nav Links-->
        <ul class="md:flex text-gray-700 text-base mr-3 origin-top"
            :class="{ 'block absolute top-20 border-b bg-white w-full p-2': showMenu, 'hidden': !showMenu}"
            id="navbar-main" x-cloak>
            <li class="px-3 cursor-pointer hover:bg-primary-soft flex items-center hover:text-gray-800" :class="showMenu && 'py-1'">
                <a href="{{ url('api/documentation') }}" target="_blank">
                    {{ __('Documentación') }}
                </a>
            </li>
            <li class="px-3 cursor-pointer hover:bg-primary-soft flex items-center hover:text-gray-800" :class="showMenu && 'py-1'">
                <a href="{{ url('http://chiquito.ws/') }}" target="_blank">
                    {{ __('Inspiración API') }}
                </a>
            </li>
            <li class="px-3 cursor-pointer hover:bg-primary-soft flex items-center hover:text-gray-800" :class="showMenu && 'py-1'">
                <a href="{{ url('https://www.frankfurter.app/') }}" target="_blank">
                    {{ __('Inspiración Web') }}
                </a>
            </li>
            <li class="px-3 cursor-pointer hover:bg-primary-soft flex items-center hover:text-gray-800" :class="showMenu && 'py-1'">
                <a href="{{ url('https://stats.uptimerobot.com/9NqKPTzpX6') }}" target="_blank">
                    {{ __('Status') }}
                </a>
            </li>
            <div class="text-xl my-auto ml-3">
                <a href="{{ url('https://gitlab.com/webmasterbrak') }}" target="_blank">
                    <i class="fa-brands fa-gitlab"></i>
                </a>
            </div>
        </ul>
    </div>
</nav>
