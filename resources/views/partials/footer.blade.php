<footer class="bg-primary-soft text-xs text-primary-dark text-center fixed inset-x-0 bottom-0 p-4 sm:text-sm">
    <div class="sm:flex sm:inline-flex flex-row">
        {{ __('Creado por ') }}
        <a href="{{ url('https://gitlab.com/webmasterbrak') }}" class="text-primary hover:text-black mx-2">
            {{ __('WebmaterBrak') }}
        </a>
        {{ __('|') }}
        {{ __('Potenciado por ') }}
        <a href="{{ url('https://alojatuweb.com') }}" class="text-primary hover:text-black mx-2">
            {{ __('AlojaTuWeb') }}
        </a>
        {{ __('|') }}
        {{ __('Contenido licenciado por ') }}
        <a href="{{ url('https://www.gnu.org/licenses/gpl-3.0.html') }}" class="text-primary hover:text-black mx-2">
            {{ __('GPLv3') }}
        </a>
        {{ __('|') }}
        <a href="{{ url('legal-disclaimer') }}" class="text-primary hover:text-black mx-2">
            {{ __('Aviso legal') }}
        </a>
        {{ __('|') }}
        <a href="{{ url('cookies-policy') }}" class="text-primary hover:text-black mx-2">
            {{ __('Política de cookies') }}
        </a>
    </div>
</footer>
