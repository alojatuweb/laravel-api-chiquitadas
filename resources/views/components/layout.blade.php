<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="dark">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Chiquitadas es una API REST de código abierto que proporciona citas y frases al estilo del gran Chiquito de la Calzada."/>
        <meta name="keywords" content="API, REST, API REST, Chiquito, Chiquito de la Calzada, frases, citas, api rest, api, rest"/>
        <meta name="author" content="Iván López Ordorica" />
        <meta name="copyright" content="AlojaTuWeb" />
        <meta name="robots" content="index, follow">
        <meta property="og:title" content="API REST del gran Chiquito de la Calzada."/>
        <meta property="og:description" content="Chiquitadas es una API REST de código abierto que proporciona citas y frases al estilo del gran Chiquito de la Calzada."/>
        <meta property="og:url" content="https://chiquitadas.es"/>
        <meta property="og:image" content="https://chiquitadas.es/Chiquito-7.png"/>
        <meta property="og:type" content="website"/>
        <meta property="og:locale" content="es_ES"/>
        <meta name="twitter:card" content="summary"/>
        <meta name="twitter:title" content="Citas y frases de Chiquito de la Calzada via API"/>
        <meta name="twitter:description" content="Chiquitadas es una API REST de código abierto que proporciona citas y frases al estilo del gran Chiquito de la Calzada."/>
        <meta name="twitter:url" content="https://www.ciquitadas.es"/>
        <meta name="twitter:image" content="https://chiquitadas.es/Chiquito-7.png"/>

        <title>{{ config('app.name') }} - @yield('title')</title>

        <link rel="icon" href="{{ asset('favicon.ico') }}">

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

        @vite('resources/css/app.css')

        <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
        <script src="https://kit.fontawesome.com/447636fad2.js" crossorigin="anonymous"></script>

        @livewireStyles
    </head>
    <body x-data="{ openMenu : false }" :class="openMenu ? 'overflow-hidden' : 'overflow-visible' " class="antialiased">
        {{ $slot }}
        @livewireScripts
    </body>
</html>
